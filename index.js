var express = require('express');
var app = express();
var PORT = process.env.PORT || 8082;

var middleware = require('./middleware.js');
app.use(middleware.logger);
/*app.get('/',middleware.requireAuthentication,function(req,res){
	res.send('Home');
});*/

app.use(express.static(__dirname+"/public"));

app.listen(PORT,function(){
	console.log('Server Started on PORT: '+PORT);
});
///